Modified the original Docker project for Shinobi CCTV to run the database in a separate container.

Moved the database initialization outside of the sh file (using docker_entrypoint_initdb.d) used inside the shinobi initialization.

Run start_image.sh as per usual and it will launch the docker-compose and set everything up.